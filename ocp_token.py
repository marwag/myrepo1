import argparse
import logging
import sys
from urllib.parse import urlparse, parse_qs
 
import requests
import urllib3
 
ERR = 1
 
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
 
logging.basicConfig(stream=sys.stdout)
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
 
oauth_server_url = "{endpoint}/.well-known/oauth-authorization-server"
authorization_url = "{authorization_endpoint}?client_id=openshift-challenging-client&response_type=token"
 
 
def get_token(endpoint, user, password):
    res = requests.get(oauth_server_url.format(endpoint=endpoint), verify=False)
    if res.status_code != requests.codes.ok:
        logger.error(f"Unable to get oauth server using {oauth_server_url.format(endpoint=endpoint)}")
        return ERR
 
    res_json = res.json()
    if "authorization_endpoint" not in res_json:
        logger.error(f"Unable to find authorization_endpoint in response payload: {res_json}")
    authorization_endpoint = res_json["authorization_endpoint"]
 
    res = requests.get(authorization_url.format(authorization_endpoint=authorization_endpoint), verify=False,
                       allow_redirects=False, auth=(user, password), headers={"X-CSRF-Token": "it-does-not-matter"})
    # Expected 302 Found
    if res.status_code != requests.codes.found:
        logger.error(f"Error calling authorization url"
                     f" {authorization_url.format(authorization_endpoint=authorization_endpoint)},"
                     f" status code: {res.status_code}")
        return ERR
    # logger.info("\n".join(res.headers))
    location = res.headers.get("Location")
    if location is None:
        logger.error(f"Unable to get token - Location header not found in authorization response obtained from"
                     f" {authorization_url.format(authorization_endpoint=authorization_endpoint)}")
        return ERR
    parsed = urlparse(location)
    the_token = parse_qs(parsed.fragment)["access_token"][0]
    return the_token
 
 
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Openshift Client")
    parser.add_argument("-e", "--endpoint", help="Openshift API server endpoint", required=True)
    parser.add_argument("-u", "--user", help="Openshift API server user", required=True)
    parser.add_argument("-p", "--password", help="Openshift API server password", required=True)
    args = parser.parse_args()
    token = get_token(args.endpoint, args.user, args.password)
    logger.info(f"token is {token}")
